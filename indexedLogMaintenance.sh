#!/bin/bash
##############################################################################
#  indexedLogMaintenance - SLM Indexed Log Maintenance Tool
#  Authors:
#              Maer Melo | mmelo@novell.com
##############################################################################

MEMORY_MIN=-Xms128m
MEMORY_MAX=-Xmx512m

banner() {
	echo "============================================================================="
	echo "                     SLM Indexed Log - Maintenance Tool"
	echo -e "=============================================================================\n"
}

usage() {
	banner
	echo "Usage: ./indexedLogMaintenance.sh [-c] [-r] <Indexed Log dir path>"
	echo "       -c"
	echo "	  Check indexed log specified"
	echo "       -r"
	echo -e "	  Rebuild indexed log specified\n"		
}

### Test parameters
if [ $# -ne 2 ]
then
	usage
	exit 2
fi

if [ $1 = "-c" ]
then
	CHECK_OPT=1
else
	if [ $1 = "-r" ]
	then
		REBUILD_OPT=1
	else
		usage
		exit 2
	fi
fi

### Main
if [ -f /etc/init.d/sentinel_log_mgr ]
then
	FINDHOME=`cat /etc/init.d/sentinel_log_mgr | grep "^APP_HOME" | sed "s/^APP_HOME=//;s/\"//g"`
	. "$FINDHOME/bin/setenv.sh"

	CLASSPATH="$ESEC_HOME/lib/ccsbase.jar"

	if [ "$CHECK_OPT" ]
	then
		if [ -f "$2"/index.sqfs ]
		then
			if [ ! -d "$2"/index ]
			then
				mkdir -p "$2"/index 
			fi
			echo "Mounting archive squash file-system"
			sudo su -c "mount -t squashfs -o loop -r "$2"/index.sqfs "$2"/index"
		fi

		banner
		echo -e "Running Index Check\n"
		exec "$ESEC_HOME/jre/bin/java" $MEMORY_MIN $MEMORY_MAX -classpath $CLASSPATH esecurity.ccs.comp.event.indexedlog.IndexedLogCheck "$2"

		echo -e "BlablablaH\n"

		if [ -f "$2"/index.sqfs ]
		then
			echo -e "\nUnmounting archive squash file-system"
			sudo su -c 'umount "$2"/index'
		fi
	fi

	if [ "$REBUILD_OPT" ]
	then
		banner
		echo -e "Running Index Rebuild\n"
		exec "$ESEC_HOME/jre/bin/java" $MEMORY_MIN $MEMORY_MAX -classpath $CLASSPATH esecurity.ccs.comp.event.indexedlog.IndexedLogRebuild "$2"
	fi
else
	banner
	echo "This is NOT a Sentinel Log Manager server"
	echo "Exiting..."
fi

exit 0

